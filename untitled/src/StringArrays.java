public class StringArrays {

    public static void main(String[] args) {
        String[] words = new String[4];

        words[0] = "Tere";
        words[1] = "Kalle";
        words[2] = "Pille";
        words[3] = "Mis teed?";

        System.out.println(words[3]); //kuidas yhe sysoutiga saada nt 1 ja 3s


        String[] fruits = {"apple", "banana", "kiwi"};

        for (String yksfruit : fruits) {
            System.out.println(yksfruit);
        }
    }


}
