public class MultiDimensionalArrays {
    public static void main(String[] args) {


        int[] v22rtused = {3, 5,  2343};

        System.out.println(v22rtused[2]);

        int[][] maatriks ={
                {2, 4, 16},
                {3, 6, 15},
                {1, 5, 7, 19}
        };

        System.out.println(maatriks[1][1]);
        System.out.println(maatriks[2][2]);

        String[][] tekstid = new String[2][3];

        tekstid[0][1] = "Hello there";

        System.out.println(tekstid[0][1]);

        for(int rida=0; rida<maatriks.length; rida++) {
            for(int tulp=0; tulp<maatriks[rida].length; tulp++){ //miks see "rida" ei suurene kui yhe korra see loop on tehtud
                System.out.print(maatriks[rida][tulp] + "\t");
            }
            System.out.println();
        }


        String[][] s6nad = new String[2][];

        s6nad[0] = new String[6];

        s6nad[0][1] = "tere";
        s6nad[0][5] = "kalle";

        System.out.println(s6nad[0][1]);
        System.out.println(s6nad[0][5]);

    }






}
