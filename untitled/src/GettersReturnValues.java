class Inimene {
    String name;
    int age;

    void speak() {

        System.out.println("My name is: " + name);

    }

    int calculateYearsToRetirement() {
        int yearsLeft = 65 - age;

        System.out.println(yearsLeft + " aastat pensionini");

        return yearsLeft;
    }

    int getVanus() {
        return age;
    }
    String getName() {
        return name;
    }

}



public class GettersReturnValues {
    public static void main(String[] args) {
        Inimene inimene1 = new Inimene();
        inimene1.name = "endel pendel";
        inimene1.age = 25;
        inimene1.speak();


        int years = inimene1.calculateYearsToRetirement();
        System.out.println(years + " aastat pead veel kannatama");

        int age = inimene1.getVanus();
        String name = inimene1.getName();

        System.out.println(age + name);


    }






}
