public class IfLoops {
    public static void main(String[] args){
        int myLoop = 0;
        int myInt = 20;

        if(myInt < 20) {
            System.out.println("Yes its true");
        }
        else if(myInt > 30){
              System.out.println("No its false)");
        }
        else{
                System.out.println("none the above");
        }

        while(true) {
            System.out.println("Looping: " + myLoop);

            if(myLoop==5){
                break;
            }
            myLoop++;
            System.out.println("Running ");
        }
    }


}
