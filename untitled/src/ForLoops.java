   public class ForLoops {
         public static void main(String[] args) {

             for(int i=0; i < 7; i++) {
                 //for int i=0 toimub enne loopi algust
                 //loop toimib kuni i < 7
                 //i++ rakendatakse peale igat konkreetset loopi tsyklit



                 System.out.println("Hello" + (i+42));
                 //kas siin saab i-le nt lahutada ka suvlist arvu
                 //mis see i++ siin teeb

                System.out.printf("the value of i is: %d ", i);
                System.out.println();
                 System.out.printf("the vaartus of i is: %d\n ", i);

             }

         }
}
