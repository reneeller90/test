public class Strings {
    public static void main(String[] args) {
        int myInt = 777;
        String tekst = "Hello World";
        //String is type of object that can hold text
        //tekst is variable which is capable of referring
        //"Hello" is text object

        String blank = " Kalle ";
        String name = "Bob";
        String greeting = tekst + blank + name;


        System.out.println(greeting);

        System.out.println(tekst);

        System.out.println("John " + "Welcome to " + tekst + blank);

        System.out.println("My integer is: " + myInt);

        double myDouble = 7.985;
        System.out.println("My number is: " + myDouble);

    }

}
