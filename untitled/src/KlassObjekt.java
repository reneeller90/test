class Persoon {
    // Instance variable(data or state)  , kas  lihtsalt klassi ei saa mujades failides kasutada?
    String nimi;
    int vanus;



    //klassid v6ivad sisaldada
    //1.Data - nt nimed, aadress, vanus vms
    //2. Subroutines(methods)

  void ytleMidagi() {
      for(int i=0; i<3; i++) {
          System.out.println("My name is " + nimi + " " + vanus);
      }
  }

  void sayHello() {
      System.out.println("Tere Tere Tere");

  }
}

class Loomad {
    String tyyp;
    int vanus;

 void loomaTutvustus() {
     System.out.println("Loomaks on " + tyyp +" ja ta vanus on " + vanus);

 }

}
public class KlassObjekt {

    public static void main(String[] args) {

        Persoon persoon1 = new Persoon();

        persoon1.nimi = "Joe Rogan";
        persoon1.vanus = 38;
        persoon1.ytleMidagi();
        persoon1.sayHello();
        Persoon persoon2 = new Persoon();

        persoon2.nimi = "Janno Pille";
        persoon2.vanus = 20;
        persoon2.ytleMidagi();

        System.out.println(persoon1.nimi);

        Loomad loom1 = new Loomad();

        loom1.tyyp = "kass";
        loom1.vanus = 5;
        loom1.loomaTutvustus();

        Loomad loom2 = new Loomad();

        loom2.tyyp = "koer";
        loom2.vanus = 2;
        loom2.loomaTutvustus();

        System.out.println(loom2.tyyp);
        System.out.println(loom1.tyyp +" ja vanus " + loom1.vanus);






    }

}
