import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
public class Hangman {
    public static void main(String[] args) {

        String s6na = "abcdefghijkl";
        Scanner sisesta = new Scanner(System.in);
        Map arvamised = new HashMap<Character,Boolean>();
        for(int i=0;i<s6na.length();i++){
            char t2ht = s6na.charAt(i);
            arvamised.put(t2ht,false);

        }
        n2itaseisu(arvamised,s6na);
        for(int i=0;i<10;) {
            char t2heke = sisesta.nextLine().toLowerCase().charAt(0);
            boolean oige = arvamised.containsKey(t2heke);

            if (oige){
                arvamised.put(t2heke, true);
                System.out.println("     on kyll seal sees");

                if(kasv6itsid(arvamised)){
                    System.out.println("v6itsid!");
                    return;
                }
            }

            else {
                joonista(i);
                i++;

                System.out.println("proovi uuesti");
            }
            n2itaseisu(arvamised,s6na);
        }
        System.out.println("Kaotasid!");


    }
    static boolean kasv6itsid(Map arvamised2) {
        boolean tulemus = arvamised2.containsValue(false);
        return !tulemus;



    }
    static void n2itaseisu(Map<Character,Boolean> arvamised3,String word){
        for(int i=0;i<word.length();i++) {
         char karakter = word.toLowerCase().charAt(i);
            boolean kasonarvatud = arvamised3.get(karakter);
            if(kasonarvatud){
                System.out.print(karakter);
            }
            else{
                System.out.print(" _ ");
            }
        }
        System.out.println();

    }
    static void joonista(int i){
        switch(i){
            case 0 :
                System.out.println("-");
                break;
            case 1 :
                System.out.println("        __\n");
                break;
            case 2 :
                System.out.println(
                        "        __\n" +
                        "         |\n");
                break;
            case 3 :
                System.out.println(
                        "        __\n" +
                        "         |\n" +
                        "         O\n");
                break;
            case 4 :
                System.out.println("        __\n" +
                        "         |\n" +
                        "         O\n" +
                        "         |\n");
                break;


            case 5 :
                System.out.println(
                        "        __\n" +
                        "         |\n" +
                        "         O\n" +
                        "        /|\n");
                break;

            case 6 :
                System.out.println(
                        "        __\n" +
                        "         |\n" +
                        "         O\n" +
                        "        /|\\\n");
                break;
            case 7 :
                System.out.println(
                        "        __\n" +
                        "         |\n" +
                        "         O\n" +
                        "        /|\\\n" +
                        "         |\n" );
                break;
            case 8 :
                System.out.println(
                        "        __\n" +
                        "         |\n" +
                        "         O\n" +
                        "        /|\\\n" +
                        "         |\n" +
                        "          \\");
                break;
            case 9 :
                System.out.println(
                        "        __\n" +
                        "         |\n" +
                        "         O\n" +
                        "        /|\\\n" +
                        "         |\n" +
                        "        / \\");
                break;

        }



    }


}
