class Robot {
     public void ytleMidagi(String tekst){  //(String tekst) on siin  meetodi parameeter , kuhu saab edasi nt Stringi , et see meetod saaks seda kasutada
         System.out.println("hello" + tekst);
     }

     public void hyppa(int heigth){
         System.out.println("Hyppan: " + heigth +"meetrit");
     }
     public void liigutus(String direction, String lamp, double meetrit, int hind){
         System.out.println("suunas: " + direction + " kasutan selleks " + lamp +"mille hind on " + hind + " hyppan meetrites " + meetrit);
     }

}


public class MeetodiParameetrid {


    public static void main(String[] args) {
         Robot robocop = new Robot();

          robocop.ytleMidagi(" Olen Robocop");
          robocop.hyppa(7);
          robocop.hyppa(6);

          String suund = "IDA";
          int suurus = 29;
          robocop.liigutus(suund , "kompass" , 23.2 , suurus );

    }



}
