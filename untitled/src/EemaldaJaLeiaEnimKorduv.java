import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class EemaldaJaLeiaEnimKorduv {
    public static void main(String[] args) {

        int[] naide = {1, 5, 3, 3, 6, 3, 7, 7}; // vastus on 7

        Map<Integer,Integer> mituNumbrit = new HashMap<>();

        for(Integer arv:naide){
            if(arv==3){
                continue;
            }
            if(mituNumbrit.containsKey(arv)) {
                int kuiMitu = mituNumbrit.get(arv) + 1;
                mituNumbrit.put(arv,kuiMitu);

            }
            else {
                mituNumbrit.put(arv,1);
            }
        }
        Map.Entry<Integer,Integer> maxEntry = null;
        Set<Map.Entry<Integer, Integer>> read = mituNumbrit.entrySet();
        for(Map.Entry<Integer,Integer> rida: read){

            if (maxEntry == null || rida.getValue()>maxEntry.getValue()){
                maxEntry = rida;
            }


       }
        System.out.println(maxEntry.getKey());
    }

}

