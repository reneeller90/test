class Frog {
    String nimi;
    int vanus;
    public void setName(String newName){

        nimi = newName;
    }
    public void setAge(int newAge){

        vanus = newAge;
    }
    public String gettNimi(){

        return nimi;
    }
    public int gettVanus(){

        return vanus;
    }

}


public class SettersAndThis {

    public static void main(String[] args) {
        Frog konn1 = new Frog();

        konn1.nimi = "Bertie";
      //  konn1.vanus = 1;
        konn1.setName("Peep");
        System.out.println("tere" + konn1.gettNimi());
        konn1.setAge(5);
//miks ta siin 2 korda ei anna ehk miks gettNimi selle uue nime annab nyyd?
        System.out.println(konn1.gettNimi());
    }




}
